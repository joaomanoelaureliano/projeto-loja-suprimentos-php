﻿<?php
	include("ADM/conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>GALERIA - FOTOS</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/topo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
			<link rel='stylesheet' href='CSS/galeriafotos.css' type="text/css">
			<link rel='stylesheet' href='CSS/rodape.css' type="text/css">
		<style type="text/css">
			.titulo{
				color: #FFFFFF;
				text-decoration: none;
				text-transform:uppercase
			}
		</style>
	</head>
	<body>
		<div class='corpo'>
			
			<!--TOPO -->	
			<div class='topo'> 
				<?php include('topo.php'); ?>
			</div>
			<!-- FIM DO TOPO -->
			
			<!-- CORPO DO MENU -->
				<?php include('menu.php'); ?>	
			<!-- FIM DO CORPO DO MENU -->
			
			<!-- CONTEUDO DA PAGINA GALERIA -->		
			<div class='conteudo' align='center'><br>
				<table cellspacing="5" cellpadding="1">
				<?php
					$id = $_GET['id'];
					$sql = "SELECT * 
							FROM catgaleria 							
							WHERE id = $id";
							
					$retorno = mysqli_query($conexao, $sql);
					
					while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
				?>				
					<tr>
						<td colspan='2' align='center' bgcolor="#c20001"><h1 class="titulo"><?php echo $obj['categoria']; ?></h1></td>
					</tr>
				<?php
					}
				?>					
				<?php
					$id = $_GET['id'];
					$sql = "SELECT * 
							FROM galeriafotos 							
							WHERE id_categoria = $id";
							
					$retorno = mysqli_query($conexao, $sql);					
					$contador = 1;
					
					while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
						
						if($contador == 1){
							   echo "<tr>";
						}
				?>
					<td><img src='<?php echo"IMG/FOTOS/GALERIA/".$obj['foto'];?>' width='500' height='400' text-align='center'></td>					
				<?php
						if($contador == 2){
							echo "</tr>";
							$contador = 1;            
						}
						else{
						$contador++;	
						}
					}
				?>
					<tr>
						<td colspan='2' align='center' bgcolor="#c20001"><h1><a class='link' href='galeria.php'>VOLTAR</a></h1></td>
					</tr>
				</table>
			</div>		
			<!-- FIM DO CONTEUDO DA PAGINA GALERIA -->
			
			<!-- RODAPÉ -->		
			<div class='rodape'>
				<?php include('rodape.php'); ?>
			</div>		
			<!-- FIM DO RODAPÉ -->		
			
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>