﻿<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>FALE CONOSCO</title>
		<link rel='stylesheet' href='CSS/faleconosco.css' type="text/css">
		<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<script type="text/javascript">
			function validar(){
				var 	nome 		= formuser.nome.value;
				var		email 		= formuser.email.value;
				var		assunto		= formuser.assunto.value;
				var		mensagem	= formuser.mensagem.value;
						
						if (nome == ""){
							alert('Campo NOME é obrigatório, para continuar preencha o campo!');
							formuser.nome.focus();
							return false;						
						}
						if (email == ""){
							alert('Campo E-MAIL é obrigatório, para continuar preencha o campo!');
							formuser.email.focus();
							return false;						
						}
						if (assunto == ""){
							alert('Campo ASSUNTO é obrigatório, para continuar preencha o campo!');
							formuser.assunto.focus();
							return false;						
						}
						if (mensagem == ""){
							alert('Campo MENSAGEM é obrigatório, para continuar preencha o campo!');
							formuser.mensagem.focus();
							return false;						
						}
			}
		</script>
	</head>
	<body>

		<div class='corpo'>
			<!-- TOPO -->	
			<div class='topo'> 
				<?php include('topo.php'); ?>
			</div>
			<!-- FIM DO TOPO -->
			
			<!-- CORPO DO MENU -->
				<?php include('menu.php'); ?>
			<!-- FIM DO CORPO DO MENU -->
			
			<!-- CONTEUDO DA PAGINA FALE CONOSCO -->
			<div class='conteudo'><br><br>	
				
					<fieldset id="contato">
						<form name='formuser' method="post" class='form' action='gravarcontato.php'>
								<h1 align='center'>FALE CONOSCO</h1><br>
								<label for="nome">NOME:</label><br>
								<input type="text" name="nome" id="nome" maxlength="200" size='50' style='border-radius: 10px' required="required"><br>
							
							
								<label for="email">E-MAIL:</label><br>
								<input type="email" name="email" id="email" maxlength="250" size='50' style='border-radius: 10px' required="required"><br>
						
							
								<label for="telefone">TELEFONE:</label><br>
								<input type="text" name="telefone" id="telefone" maxlength="14" size='50' style='border-radius: 10px' required="required"><br>
							
								<label for="celular">CELULAR:</label><br>
								<input type="text" name="celular" id="celular" maxlength="14" size='50' style='border-radius: 10px' required="required"><br>
								<?php  
									date_default_timezone_set('America/Sao_Paulo');
									$date = date('d/m/Y H:i');						
								?>
						
								<label for="horacontato">DATA/HORA CONTATO:</label><br>
								<td><input type="text" name="horacontato" value="<?php echo $date; ?>" id="horacontato" size='50' style='border-radius: 10px' required="required"><br>
						
								<label for="assunto">ASSUNTO:</label><br>
								<input type="text" name="assunto" id="assunto"maxlength="200" size='50' style='border-radius: 10px' required="required"><br>
						
								<label for="mensagem">MENSAGEM:</label><br>
								<textarea name="mensagem" id="mensagem" class='textarea' maxlength="500" style='border-radius: 10px' required="required"></textarea>
							
								<input id="btn" type="submit" Onclick="return validar()" value="Enviar Mensagem">
								<?php session_start(); // Inicia a sessão
										session_destroy(); // Destrói a sessão limpando todos os valores salvos
										?>
								<input id="btn" type="reset" name="limpar" value="Limpar">
									
						</form>
					</fieldset>
			</div>
			<!-- RODAPÉ -->		
			<div class='rodape'>
				<?php include('rodape.php'); ?>
			</div>		
			<!-- FIM DO RODAPÉ -->
		</div>
	</body>
</html>
