﻿<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>QUEM SOMOS</title>		
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/topo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
			<link rel='stylesheet' href='CSS/quemsomos.css' type="text/css">
			<link rel='stylesheet' href='CSS/rodape.css' type="text/css">
	</head>
	<body>
		<div class='corpo'>
		
			<!--TOPO -->	
			<div class='topo'> 
				<?php include('topo.php'); ?>
			</div>
			<!-- FIM DO TOPO -->
			
			<!-- MENU -->		 
				<?php include('menu.php'); ?>			
			<!-- FIM DO MENU -->
		
			<!-- CONTEUDO DA PAGINA QUEM SOMOS -->	
			<div class='conteudo' align='center'>
				<div width='400'>
					<br>
					<h1> QUEM SOMOS </h1> <br>
					<p class='quemsomos'> 

						<p>A RX Suplementos é a maior loja online exclusiva de suplementos alimentares do  Brasil.</p><br>

						<p>E manter a liderança de um mercado que cresce muito em todo país, 
						<br>com diversos concorrentes e mudanças de cenário, sejam elas econômicas ou peculiares do segmento, não é tarefa fácil.</p><br>

						<p>Contamos com departamentos de tecnologia da informação e criação internos, 
						<br>que acompanham as novidades e tendências mundiais, e que permitem proporcionar aos nossos clientes uma navegação prática, rápida, 
						<br>e principalmente segura, com constantes atualizações. Nosso site conta com excelente nível de proteção, e é certificado por renomadas empresas de segurança online, como SiteBlindado, Comodo e Fcontrol.</p><br>

						<p>Na parte de atendimento ao consumidor contamos com profissionais especializados, 
						<br>que passam por treinamentos e atualizações frequentes e são supervisionados por nutricionistas, 
						<br>proporcionando ao cliente uma excelente experiência de atendimento, seja através de e-mail, chat online, ou televendas.</p><br>

						<p>A maior parte do processo de separação de pedidos e postagem é automatizada, permitindo uma enorme agilidade e assertividade.</p><br>

						<p>Todos produtos comercializados são autorizados pela Anvisa e todas as vendas são emitidas com notas fiscais, 
						<br>garantindo assim a procedência de todos itens.</p><br> 
					</p>
				</div>
					<br><br>
				<table class='icones'>
					<tr>
						<td width='100' align='center'><img src='IMG/ICONES/QUEMSOMOS/volei.png' width='100'></td>
						<td width='100' align='left'><img src='IMG/ICONES/QUEMSOMOS/patins.png' width='100'></td>
						<td width='100' align='center'><img src='IMG/ICONES/QUEMSOMOS/peso.png' width='100'></td>
						<td width='100' align='center'><img src='IMG/ICONES/QUEMSOMOS/futebol.png' width='100'></td>
					</tr>
				</table>				
			</div>	
			<!-- FIM DO CONTEUDO DA PAGINA QUEM SOMOS -->
		
			<!-- RODAPÉ -->		
			<div class='rodape'>
				<?php include('rodape.php'); ?>
			</div>		
			<!-- FIM DO RODAPÉ -->	
		</div>
	</body>
</html>