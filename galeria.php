<?php
	include("ADM/conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>GALERIA</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/topo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
			<link rel='stylesheet' href='CSS/galeria.css' type="text/css">
			<link rel='stylesheet' href='CSS/rodape.css' type="text/css">
			
		<style type="text/css">
			.titulo{
				color: #FFFFFF;
				text-decoration: none;
				text-transform:uppercase
			}			
		</style>			
	</head>
	<body>
		<div class='corpo'>
			
			<!-- TOPO -->	
			<div class='topo'> 
				<?php include('topo.php'); ?>
			</div>
			<!-- FIM DO TOPO -->
			
			<!-- MENU -->		 
				<?php include('menu.php'); ?>			
			<!-- FIM DO MENU -->
			
			<!-- CONTEUDO DA PAGINA GALERIA -->		
			<div class='conteudo' align='center'><br>
				<table cellspacing="5" cellpadding="1">
				
				<?php
					$sql = "SELECT * FROM catgaleria";
					$retorno = mysqli_query($conexao, $sql);
					
					while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
				?>
					<tr>
						<td colspan='2' align='center' bgcolor="#c20001"><h1 class="titulo"><a href='galeriafotos.php?id=<?php echo $obj['id']; ?>'><?php echo $obj['categoria']; ?></a></h1></td>
					</tr>				
				<?php
					}
				?>
				</table>
			</div>
			<!-- FIM DO CONTEUDO DA PAGINA GALERIA -->
			
			<!-- RODAPÉ -->		
			<div class='rodape'>
				<?php include('rodape.php'); ?>
			</div>		
			<!-- FIM DO RODAPÉ -->		
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>
