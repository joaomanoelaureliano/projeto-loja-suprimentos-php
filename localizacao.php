﻿<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>LOCALIZAÇÃO</title>
		<link rel='stylesheet' href='CSS/localizacao.css' type="text/css">
		<link rel='stylesheet' href='CSS/menu.css' type="text/css">
	</head>
<body>
	<div class='corpo'>	
		<!-- TOPO -->	
		<div class='topo'> 
			<?php include('topo.php'); ?>
		</div>
		<!-- FIM DO TOPO -->
		
		<!-- MENU -->		 
			<?php include('menu.php'); ?>			
		<!-- FIM DO MENU -->
		
		<!-- PAGINA LOCALIZAÇÃO -->		
		<div class='conteudo' align='center'> 	
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3500.081632576438!2d-49.336687085292034!3d-28.687204582396266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x952178e79469f363%3A0x47308fec05d034c8!2zTmHDp8O1ZXMgU2hvcHBpbmc!5e0!3m2!1spt-BR!2sbr!4v1493569257477" 
					width="1024" 
					height="400" 
					frameborder="0" 
					style="border:0" 
					allowfullscreen>
			</iframe>
		</div>		
		<!-- FIM DO CONTEUDO DA PAGINA LOCALIZAÇÃO -->
		
		<!-- INÍCIO DO RODAPÉ -->		
		<div class='rodape'>
			<?php include('rodape.php'); ?>
		</div>		
		<!-- FIM DO RODAPÉ -->	
	</div>
</body>
</html>


