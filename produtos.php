<?php
	include("ADM/conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>PRODUTOS</title>
		<link rel='stylesheet' href='CSS/produtos.css' type="text/css">
		<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<link rel='stylesheet' href='CSS/rodape.css' type="text/css">
	</head>
<body>	
	<div class='corpo'>
	
		<!-- TOPO -->	
		<div class='topo'> 
			<?php include('topo.php'); ?>
		</div>
		<!-- FIM DO TOPO -->
		
		<!-- MENU -->		 
			<?php include('menu.php'); ?>			
		<!-- FIM DO MENU -->
	
		<!-- CONTEUDO DA PAGINA PRODUTOS -->
		<div class='conteudo'>
			<div id='produtos-cat'>
			<!-- PRODUTOS-->	
				<?php 
					$sql = " SELECT * FROM catproduto";				
					$retorno = mysqli_query($conexao, $sql);
					
					while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)) {
				 ?>
					<div class="wrap-cat">
						<div class="title-cat"><?php echo $obj['categoria']; ?></div>
						<div class="wrap-produtos">
							<?php
								$sql_prod = " SELECT * FROM subcatproduto WHERE id_categoria = '$obj[id]'";
				
								$query_prod = mysqli_query($conexao, $sql_prod);
								
								while($prod = mysqli_fetch_array($query_prod, MYSQLI_ASSOC)) {
							?>
							<div class="prod">
								<?php echo $prod['subcategoria']; ?>
							</div>
							<?php
								}
							?>
						</div>
					</div>
					
				<?php } ?>
				<!-- SUBPRODUTOS -->
			</div>
		</div>		
		<!-- FIM DO CONTEUDO DA PAGINA PRODUTOS	-->
	</div><br>
	<!-- RODAPÉ -->		
	<br><div class='rodape'>
		<?php include('rodape.php'); ?>
	</div>		
	<!-- FIM DO RODAPÉ -->
</body>
</html>
<?php
	mysqli_close($conexao);
?>