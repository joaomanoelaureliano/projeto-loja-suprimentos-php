﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>LISTAR MENSAGENS</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
			<link rel='stylesheet' href='CSS/rodape_listagem.css' type="text/css">
		<style>
	
		/* CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:1024px;
				position: absolute;
				margin-top: 40px;
				margin-left: 200px;
			}			
			.tbconteudo{
				width:600;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
				border-color: #ff0000;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/*  LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 30px;
				border-collapse: collapse;
				border-color: #ff0000;
			}
			.listagem td{
				border-color:#363636;
				padding: 0px 10px 0px 10px;
				text-align: left;
			}
			.listagem th{
				border-color:#363636;
				padding: 0px 10px 0px 10px;
				width: 50px;
				text-align: right;
			}
			.opcao td{
				height: 50px;
			}			
			.opcao td a{
				text-decoration: none;
				color: #e74c3c;
				font-weight: lighter;
				text-align: center;
			}
			.opcao td a:hover{
				color: #27ae60;
			}
			.linha{
				background-color: #bdc3c7;
			}
		/* ---------------FIM--------------- */	
			
		</style>
	</head>
	<body>
		<div class='corpo'>
			<div class='menu'>
				<?php include('menu.php') ?>
				<div class='conteudo'>
					<table class='tbconteudo'>
						<tr>
							<th colspan="12" class='titulo'>MENSAGENS RECEBIDAS</th>
						</tr>
					<?php
						$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1; //VERIFICA A PÁGINA
						$sql = "SELECT * FROM contato ORDER BY id desc";
						$retorno = mysqli_query($conexao, $sql);
						
						//INÍCIO DA PAGINAÇÃO
						$total = mysqli_num_rows($retorno);
						$registros = 5;
						$numPaginas = ceil($total/$registros);
						$inicio = ($registros*$pagina)-$registros;
						
						//FAZ A CONSULTA PARA PAGINAR
						$sql = "SELECT * FROM contato ORDER BY id desc limit $inicio,$registros"; 
						$retorno = mysqli_query($conexao, $sql); 
						$total = mysqli_num_rows($retorno);
						
						while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
					?>
						<tr class='listagem'>
							<th>Nome</th>
							<td colspan='3'><?php echo $obj['nome']; ?></td>
						</tr>
						<tr class='listagem'>
							<th>E-mail</th>
							<td colspan="4"><?php echo $obj['email']; ?></td>
						</tr>
						<tr class='listagem'>
							<th>Telefone</th>
							<td><?php echo $obj['telefone']; ?></td>
							<th>Celular</th>
							<td><?php echo $obj['celular']; ?></td>
						</tr>
						<tr class='listagem'>
							<th>Data/Hora</th>
							<td colspan='3'><?php echo $obj['horacontato']; ?></td>	
						</tr>							
						<tr class='listagem'>
							<th>Assunto</th>
							<td colspan="4"><?php echo $obj['assunto']; ?></td>
						</tr>
						<tr class='listagem'>
							<th valign='top'>Mensagem</th>
							<td colspan="4" valign='top'><?php echo $obj['mensagem']; ?></td>
						</tr>
						<tr class='opcao'>
							<td colspan="4"><a href="excluir_mensagem.php?id=<?php echo $obj['id']; ?>">Excluir Mensagem</a></td>				
						</tr>
						<tr class='linha'>
							<td colspan="4"></td>				
						</tr>
					<?php
						}
					?>
						<tr>
							<th colspan="12" class='totregistro'>Existe(m) <?php echo mysqli_num_rows($retorno); ?> registro(s) nesta página</th>
						</tr>					
						<tr>
							<th colspan="12" class='totpagina'>Página: 
							<?php
								//EXIBE A PAGINAÇÃO
								for($i = 1; $i < $numPaginas + 1; $i++) { 
									echo "<a href='listar_mensagens.php?pagina=$i'>".$i."</a> "; 
								} 
							?>
							</th>
						</tr>						
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>