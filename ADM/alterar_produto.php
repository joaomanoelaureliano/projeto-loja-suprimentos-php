﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>ALTERAR PRODUTO</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<style>
		
		/* CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:1024px;
				position: absolute;
				margin-left: 220px;
				margin-top: 40px;
			}			
			.tbconteudo{
				width:500;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/* LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 50px;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}
			.tdlabel{
				text-align: right;
				width:150px;	
				padding-right: 10px;				
			}
			.tdinput{
				text-align: left;
				padding-left: 10px;
			}
			.btn{
				width: 150px;				
			}
		/* ---------------FIM--------------- */	
				
		</style>
		<script type="text/javascript">
			function validar(){
				var 	id_subcategoria 	= formuser.id_subcategoria.value;
						
						if (id_subcategoria == ""){
							alert('Campo SUBCATEGORIA é obrigatório, para cadastrar preencha o campo!');
							formuser.id_subcategoria.focus();
							return false;						
						}
			}
		</script>
	</head>
	<body>
		<?php
			$id = $_GET['id'];
			$sql = "SELECT * FROM produtos WHERE id = $id";
			$retorno = mysqli_query($conexao, $sql);
			$prod = mysqli_fetch_array($retorno, MYSQLI_ASSOC);			
		?>
		<div class='corpo'>
			<div class='menu'>
				<?php include('menu.php') ?>
				<div class='conteudo'>
					<table class='tbconteudo'>
					<form action="alterar_produto_db.php" method="post" name='formuser'>	
						<input type="hidden" name="id" value="<?php echo $id; ?>">					
						<tr>
							<th colspan="3" class='titulo'>ALTERAR PRODUTO</th>
						</tr>
						<tr class='listagem'>
							<td class='tdlabel'><label for="id_subcategoria">Subcategoria:</label></td>
							<td class='tdinput'>
								<select name="id_subcategoria" id="id_subcategoria">
									<?php
										$sql = "SELECT * FROM subcatproduto";
										$retorno = mysqli_query($conexao, $sql);
										while($subcat = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
									?>
									<option value="<?php echo $subcat['id']; ?>" <?php if($prod['id_subcategoria'] == $subcat['id']) { ?>selected="selected"<?php } ?>><?php echo 	$subcat['subcategoria']; ?></option>
									<?php
										}
									?>
								</select>
							</td>
						</tr>
						<tr class='listagem'>
							<td class='tdlabel'><label for="produto">Produto:</label></td>
							<td class='tdinput'><input type="text" name="produto" id="produto" value="<?php echo $prod['produto']; ?>" maxlength="100"></td>
						</tr class='listagem'>
						<tr class='listagem'>
							<td colspan='2'><input Onclick="return validar()" type="submit" value="Alterar" class='btn'></td>	
						</tr>
						<tr class='listagem'>
							<td colspan='2'><a href="listar_produto.php">Voltar</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>