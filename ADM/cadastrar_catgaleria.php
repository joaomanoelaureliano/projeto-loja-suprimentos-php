﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>CADASTRAR CATEGORIA</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<style>
		
		/* CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:1024px;
				position: absolute;
				margin-left: 220px;
				margin-top: 40px;
			}			
			.tbconteudo{
				width:500;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/* LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 50px;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}
			.tdlabel{
				text-align: right;
				width:150px;	
				padding-right: 10px;				
			}
			.tdinput{
				text-align: left;
				padding-left: 10px;
			}
			.btn{
				width: 150px;				
			}
		/* ---------------FIM--------------- */	
				
		</style>
		<script type="text/javascript">
			function validar(){
				var 	categoria 	= formuser.categoria.value;
						
						if (categoria == ""){
							alert('Campo CATEGORIA é obrigatório, para cadastrar preencha o campo!');
							formuser.categoria.focus();
							return false;						
						}
			}
		</script>
	</head>
	<body>
		<div class='corpo'>
			<div class='menu'>
				<?php include('menu.php') ?>
				<div class='conteudo'>
					<table class='tbconteudo'>
					<form action="cadastrar_catgaleria_db.php" method="post" name='formuser'>						
						<tr>
							<th colspan="3" class='titulo'>CADASTRAR CATEGORIA</th>
						</tr>
						<tr class='listagem'>
							<td class='tdlabel'><label for="categoria">Categoria:</label></td>
							<td class='tdinput'><input type="text" name="categoria" id="categoria" maxlength="100"></td>
						</tr class='listagem'>
						<tr class='listagem'>
							<td colspan='2'><input Onclick="return validar()" type="submit" value="Cadastrar" class='btn'></td>	
						</tr>
						<tr class='listagem'>
							<td colspan='2'><a href="menu_galeria.php">Voltar</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>