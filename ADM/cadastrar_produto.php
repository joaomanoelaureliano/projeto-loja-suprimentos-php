﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>CADASTRAR PRODUTO</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<style>
		
		/* CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:1024px;
				position: absolute;
				margin-left: 220px;
				margin-top: 40px;
			}			
			.tbconteudo{
				width:500;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/* LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 50px;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}
			.tdlabel{
				text-align: right;
				width:150px;	
				padding-right: 10px;				
			}
			.tdinput{
				text-align: left;
				padding-left: 10px;
			}
			.btn{
				width: 150px;				
			}
		/* ---------------FIM--------------- */	
				
		</style>
		<script type="text/javascript">
			function validar(){
				var 	id_subcategoria 	= formuser.id_subcategoria.value;
				var 	produto 			= formuser.produto.value;
						
						if (id_subcategoria == ""){
							alert('Campo SUBCATEGORIA é obrigatório, para cadastrar preencha o campo!');
							formuser.id_subcategoria.focus();
							return false;						
						}
						if (produto == ""){
							alert('Campo PRODUTO é obrigatório, para cadastrar preencha o campo!');
							formuser.produto.focus();
							return false;						
						}
			}
		</script>
	</head>
	<body>
		<div class='corpo'>
			<div class='menu'>
				<?php include('menu.php') ?>
				<div class='conteudo'>
					<table class='tbconteudo'>
					<form action="cadastrar_produto_db.php" method="post" name='formuser'>						
						<tr>
							<th colspan="3" class='titulo'>CADASTRAR PRODUTO</th>
						</tr>
						<tr class='listagem'>
							<td class='tdlabel'><label for="id_subcategoria">Subcategoria:</label></td>
							<td class='tdinput'>
								<select name="id_subcategoria" id="id_subcategoria">
									<?php
										$sql = "SELECT * FROM subcatproduto";
										$retorno = mysqli_query($conexao, $sql);
										while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
									?>				
									<option value="<?php echo $obj['id']; ?>"><?php echo $obj['subcategoria']; ?></option>
									<?php
										}
									?>
								</select>
							</td>
						</tr>
						<tr class='listagem'>
							<td class='tdlabel'><label for="produto">Produto:</label></td>
							<td class='tdinput'><input type="text" name="produto" id="produto" maxlength="100"></td>
						</tr>
						<tr class='listagem'>
							<td colspan='2'><input Onclick="return validar()" type="submit" value="Cadastrar" class='btn'></td>	
						</tr>
						<tr class='listagem'>
							<td colspan='2'><a href="menu_produtos.php">Voltar</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>