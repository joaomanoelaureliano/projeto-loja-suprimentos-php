<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>CADASTRAR FOTO</title>
		<style>
			body{
				background: url("IMG/FUNDO/fundo.jpg") no-repeat center fixed;
				font-family: Arial;				
			}
			.corpo{
				width:300px;
				height:300px;
				position:absolute;
				top:40%;
				left:45%;
				margin-top:-40px;
				margin-left:-45px;
			}
			table{
				width:250px;
				height:200px;
				background-color: #ecf0f1;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
				border-color: #bdc3c7;
			}
			.listagem{
				font-weight: lighter;
				font-size: 18px;
				height: 50px;
			}
			.listagem td{
				font-size: 18px;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}			
		</style>
	</head>
	<body>
		<div class='corpo'>
			<table>
					<tr>
						<td>
							<?php
								$foto	= $_POST['foto'];
								$sql = "INSERT INTO homefotos VALUES (null, '$foto')";
								
								mysqli_query($conexao, $sql);
								
								$erro = mysqli_error($conexao);
								if(!$erro) {
									echo "Foto cadastrada com sucesso!";
								} else {
									echo "Não foi possível cadastrar a foto! $erro";
								}
							?>
						</td>
					</tr>
					<tr class='listagem'> 
						<td><a href="cadastrar_homefotos.php">Voltar</a></td>
					</tr>
			</table>
		<div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>