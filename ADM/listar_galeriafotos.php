﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>LISTAR FOTOS GALERIA</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
			<link rel='stylesheet' href='CSS/rodape_listagem.css' type="text/css">
		<style>
		
		/*  CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:1024px;
				position: absolute;
				margin-left: 100px;
				margin-top: 40px;
			}
			
			.tbconteudo{
				width:800;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/* LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 50px;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}
			.listagem td img{
				width: 300px;
			}

		/* ---------------FIM--------------- */	
				
		</style>
	</head>
	<body>
		<div class='corpo'>
			<div class='menu'>
				<?php include('menu.php') ?>
				<div class='conteudo'>
					<table class='tbconteudo'>
						<tr>
							<th colspan="5" class='titulo'>GALERIA - FOTOS</th>
						</tr>
						<tr class='listagem'>
							<th>Código</th>
							<th>Categoria</th>
							<th>Foto</th>
							<th colspan="2">Opções</th>
						</tr>
					<?php
						$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1; //VERIFICA A PÁGINA
						$sql = "SELECT 	galeriafotos.*, 
								catgaleria.categoria 
								FROM galeriafotos
								INNER JOIN catgaleria ON(catgaleria.id = galeriafotos.id_categoria)					
								ORDER BY galeriafotos.id";
								
						$retorno = mysqli_query($conexao, $sql);
						
						//INÍCIO DA PAGINAÇÃO
						$total = mysqli_num_rows($retorno);
						$registros = 10;
						$numPaginas = ceil($total/$registros);
						$inicio = ($registros*$pagina)-$registros;
						
						//FAZ A CONSULTA PARA PAGINAR
						$sql = "SELECT 	galeriafotos.*, 
								catgaleria.categoria 
								FROM galeriafotos
								INNER JOIN catgaleria ON(catgaleria.id = galeriafotos.id_categoria)					
								ORDER BY galeriafotos.id limit $inicio,$registros";
								
						$retorno = mysqli_query($conexao, $sql); 
						$total = mysqli_num_rows($retorno);
						
						while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
					?>
						<tr class='listagem'>
							<td><?php echo $obj['id']; ?></td>
							<td><?php echo $obj['categoria']; ?></td>
							<td><img src='<?php echo"../IMG/FOTOS/GALERIA/".$obj['foto'];?>'></td>
							<td colspan="2"><a href="excluir_galeriafotos.php?id=<?php echo $obj['id']; ?>">Excluir</a></td>				
						</tr>
					<?php
						}
					?>
						<tr class='listagem'>
							<td colspan='5'><a href="menu_galeria.php">Voltar</a></td>				
						</tr>
						<tr>
							<th colspan="5" class='totregistro'>Existe(m) <?php echo mysqli_num_rows($retorno); ?> registro(s) nesta página</th>
						</tr>					
						<tr>
							<th colspan="5" class='totpagina'>Página: 
							<?php
								//EXIBE A PAGINAÇÃO
								for($i = 1; $i < $numPaginas + 1; $i++) { 
									echo "<a href='listar_galeriafotos.php?pagina=$i'>".$i."</a> "; 
								} 
							?>
							</th>
						</tr>						
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>