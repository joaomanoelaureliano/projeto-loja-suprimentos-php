﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>CADASTRAR FOTO GALERIA</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<style>		
		/* CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:1024px;
				position: absolute;
				margin-left: 220px;
				margin-top: 40px;
			}			
			.tbconteudo{
				width:500;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/* LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 50px;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}
			.tdlabel{
				text-align: right;
				width:150px;	
				padding-right: 10px;				
			}
			.tdinput{
				text-align: left;
				padding-left: 10px;
			}
			.btn{
				width: 150px;				
			}
		/* ---------------FIM--------------- */	
				
		</style>
		<script type="text/javascript">
			function validar(){
				var 	id_categoria 	= formuser.id_categoria.value;
				var 	foto 		= formuser.foto.value;
				
						if (id_categoria == ""){
							alert('Campo CATEGORIA é obrigatório, para cadastrar preencha o campo!');
							formuser.id_categoria.focus();
							return false;						
						}
						if (foto == ""){
							alert('Campo FOTO é obrigatório, para cadastrar preencha o campo!');
							formuser.foto.focus();
							return false;						
						}
			}
		</script>
	</head>
	<body>
		<?php
			if(@$_GET['erro']) {
				echo "Não foi possível cadastrar a foto! ". $_GET['erro'];
			}
		?>
		<div class='corpo'>
			<div class='menu'>
				<?php include('menu.php') ?>
				<div class='conteudo'>
					<table class='tbconteudo'>
					<form action="cadastrar_galeriafotos_db.php" method="post" name='formuser' >						
						<tr>
							<th colspan="3" class='titulo'>CADASTRAR FOTO</th>
						</tr>
						<tr class='listagem'>
							<td class='tdlabel'><label for="categoria">Categoria:</label></td>
							<td class='tdinput'>
								<select name="id_categoria" id="id_categoria">
									<?php
										$sql = "SELECT * FROM catgaleria";
										$retorno = mysqli_query($conexao, $sql);
										while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){
									?>				
									<option value="<?php echo $obj['id']; ?>"><?php echo $obj['categoria']; ?></option>
									<?php
										}
									?>
								</select>
							</td>
						</tr>						
						<tr class='listagem'>
							<td class='tdlabel'><label for="foto">Foto:</label></td>
							<td class='tdinput'><input type="file" name="foto" id="foto" maxlength="255"></td>
						</tr>
						<tr class='listagem'>
							<td colspan='2'><input type="submit" value="Cadastrar" class='btn' Onclick="return validar()"></td>	
						</tr>
						<tr class='listagem'>
							<td colspan='2'><a href="menu_galeria.php">Voltar</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>