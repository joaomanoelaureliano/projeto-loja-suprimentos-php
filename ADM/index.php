﻿<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>AREA ADMINISTRATIVA</title>
		<style>
			body{
				background: url("IMG/FUNDO/fundo.jpg") no-repeat center fixed;
				font-family: Arial;
				font-size: 14px;
			}
			.corpo{
				width:300px;
				height:300px;
				position:absolute;
				top:40%;
				left:45%;
				margin-top:-40px;
				margin-left:-45px;
				color: red;
			}
			table{
				width:260px;
				height:260px;
				background-color: #ecf0f1;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
				border-color: #bdc3c7;
			}
			.dir{				
				text-align: right;
			}
			.btn{
				width: 150px;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: lighter;
				font-size: 16px;
			}			
		</style>
	</head>
	<body>
		<div class='corpo'>
			<?php 
				if(@$_GET['erro'] == 1) {
					echo "Usuário ou senha inválida!";
				} elseif (@$_GET['erro'] == 2) {
					echo "Usuário está inativo!";
				} 
			?>
			<table>
				<form action="login_db.php" method="post">
					<tr> 
						<td colspan="2" class='titulo'> ÁREA ADMINISTRATIVA </td>
					</tr>
					<tr>
						<td class='dir'>LOGIN:</td>
						<td><input type="text" name="login" id="login" maxlength="50"></td>
					</tr>
					<tr>
						<td class='dir'>SENHA:</td>
						<td><input type="password" name="senha" id="senha" maxlength="100"> </td>
					</tr>
					<tr> 
						<td colspan="2"><input type="submit" value="Entrar" class='btn'></td>
					</tr>
					<tr>		
				</form>
			</table>
		<div>
	</body>
</html>