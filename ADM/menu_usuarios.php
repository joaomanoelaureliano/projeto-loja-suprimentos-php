﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>MENU - USUÁRIOS</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<style>
		
		/* CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:600px;
				position: absolute;
				margin-left: 300px;
				margin-top: 40px;
			}
			
			.tbconteudo{
				width:400;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/* LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 50px;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}

		/* ---------------FIM--------------- */	
					
		</style>
	</head>
	<body>
		<div class='corpo'>
			<div class='menu'>
				<?php include('menu.php') ?>
				<div class='conteudo'>
					<table class='tbconteudo'>
						<tr>
							<th colspan="2" class='titulo'>MENU - USUÁRIOS</th>
						</tr>
						<tr class='listagem'>
							<th>OPÇÃO</th>
							<th>AÇÃO</th>
						</tr>	
						<tr class='listagem'>
							<td>Usuários</td>
							<td><a href="listar_usuarios.php">Listar</a> - <a href="cadastrar_usuarios.php">Cadastrar</a></td>				
						</tr>					
					</table>
				</div>
			</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>