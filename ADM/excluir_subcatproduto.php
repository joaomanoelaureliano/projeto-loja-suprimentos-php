﻿<?php
	include("conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>EXCLUIR SUBCATEGORIA PRODUTO</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
		<style>
		
		/* CONTEÚDO DO LAYOUT */
		
			.conteudo{
				width:1024px;
				position: absolute;
				margin-left: 220px;
				margin-top: 40px;
			}			
			.tbconteudo{
				width:500;
				background-color: white;
				text-align: center;
				border-radius: 10px;
				border-collapse: collapse;
			}
			.titulo{
				background-color: rgb(0,0,0,0.4);
				border-radius: 10px 10px 0px 0px;
				color: white;
				font-weight: bold;
				font-size: 16px;
				height: 50px;
			}
			
		/* ---------------FIM--------------- */
			
		/* LISTAGEM DOS ARQUIVOS */	
			
			.listagem{
				font-weight: lighter;
				font-size: 14px;
				height: 50px;
			}
			.listagem td{
				text-align: center;
			}
			.listagem td a{
				text-decoration: none;
				color: black;
			}
			.listagem td a:hover{
				color: #e74c3c;
			}
			.btn{
				width: 150px;				
			}
		/* ---------------FIM--------------- */	
				
		</style>
	</head>
	<body>
		<?php
			$id = $_GET['id'];
		?>
		<div class='corpo'>
				<div class='conteudo'>
					<table class='tbconteudo'>
					<form action="excluir_subcatproduto_db.php" method="post">
						<input type="hidden" name="id" value="<?php echo $id; ?>">
						<tr>
							<th class='titulo'>EXCLUIR SUBCATEGORIA - PRODUTO</th>
						</tr>
						<tr class='listagem'>
							<td>Confirma exclusão da subcategoria?</td>
						</tr>
						<tr class='listagem'>
							<td><input type="submit" value="Excluir" class='btn'></td>	
						</tr>
						<tr class='listagem'>
							<td><a href="listar_subcatproduto.php">Voltar</a></td>
						</tr>
					</form>
					</table>
				</div>
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>