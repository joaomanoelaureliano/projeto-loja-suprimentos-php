﻿<?php
	include("ADM/conexao.php");
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>HOME</title>
			<link rel='stylesheet' href='CSS/corpo.css' type="text/css">
			<link rel='stylesheet' href='CSS/topo.css' type="text/css">
			<link rel='stylesheet' href='CSS/menu.css' type="text/css">
			<link rel='stylesheet' href='CSS/home.css' type="text/css">
			<link rel='stylesheet' href='CSS/rodape.css' type="text/css">
	</head>
	<body>
		<div class='corpo'>
			<!-- TOPO -->	
			<div class='topo'> 
				<?php include('topo.php'); ?>
			</div>
			<!-- FIM DO TOPO -->
			
			<!-- MENU -->
				<?php include('menu.php'); ?>
				<?php
			session_start();
			setcookie('Loja de suprementos online', 'Coookie');
			setcookie('Contato', 'João Manoel Aureliano', time() + 10000); ?>
			<!-- FIM DO MENU -->
			
			<!-- CONTEUDO DA PAGINA HOME -->		
			<div class='conteudo' align='center'>
				<?php							
					$sql = "SELECT * FROM homefotos ORDER BY RAND() LIMIT 0,4";
					$retorno = mysqli_query($conexao, $sql);
					
					while($obj = mysqli_fetch_array($retorno, MYSQLI_ASSOC)){		
				?>					
					<img src='<?php echo'IMG/FOTOS/HOME/'.$obj['foto'];?>' width='510' height='300' text-align='center'>
				<?php
					}
				?>
			</div>		
			<!-- FIM DO CONTEUDO DA PAGINA HOME -->
			
			<!-- RODAPÉ -->		
			<div class='rodape'>
				<?php include('rodape.php'); ?>
			</div>		
			<!-- FIM DO RODAPÉ -->		
		</div>
	</body>
</html>
<?php
	mysqli_close($conexao);
?>